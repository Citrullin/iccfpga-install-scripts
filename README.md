# Crypto core module bash scripts

## Dependencies

- git

## install_full_toolchain.sh

Installs all dependencies necessary to be able to flash the crypto core module and
to run the rest-server. `install_full_toolchain.sh` must be executed with root rights.
```sudo ./install_full_toolchain.sh```

## install_light_toolchain.sh

Also installs all dependencies. The only difference between light and full is the riscv toolchain.
The full version contains a complete riscv toolchain, which is only necessary when the RPi is used as
flash device for vivado. If you only use the RPi to flash and porgram and don't syntisyse a bitstream in Vivado,
use the light version.
```sudo ./install_light_toolchain.sh```