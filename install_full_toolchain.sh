#!/bin/bash

INSTALL_ROOT=$(pwd)

echo "Clone riscc-gnu-toolchain"
git clone --recursive https://github.com/riscv/riscv-gnu-toolchain riscv-gnu-toolchain

echo "Initialize submodules..."
git submodule update --init --recursive

echo "Update source list..."
apt update

echo "Install dependencies..."
apt install -y libcurl4-gnutls-dev libsystemd-dev libmicrohttpd-dev \
    libtool automake libusb-1.0.0-dev texinfo libusb-dev libyaml-dev pkg-config \
    autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison \
    flex texinfo gperf patchutils bc zlib1g-dev cmake libmicrohttpd-dev picocom

echo "Entering riscv-gnu-toolchain"
cd riscv-gnu-toolchain || (echo "Failed to cd into riscv-gnu-toolchain" && exit)
echo "Configure riscv-gnu-toolchain"
ARCH=rv32im
rmdir -rf $ARCH
mkdir $ARCH
cd $ARCH || (echo "Failed to cd into $ARCH" && exit)
../configure  --prefix=/opt/$ARCH --with-arch=$ARCH --with-abi=ilp32
echo "Compile riscv-gnu-toolchain"
make CFLAGS_FOR_TARGET_EXTRA="-O2" CXXFLAGS_FOR_TARGET_EXTRA="-O2" -j4
echo "Leaving riscv-gnu-toolchain"
cd "$INSTALL_ROOT" || (echo "Failed to cd into installation root" && exit)
echo "$INSTALL_ROOT"
echo "riscv-gnu-toolchain successfully compiled"

echo "Entering openocd_riscv directory"
cd iccfpga-utils/openocd_riscv || (echo "Failed to cd into openocd_risc" && exit)
echo "Bootstrap OpenOCD.."
./bootstrap
echo "Configure OpenOCD"
./configure --enable-bcm2835gpio --enable-ftdi --enable-dummy --enable-sysfsgpi
echo "Compile OpenOCD..."
make
echo "Install OpenOCD..."
make install
echo "Leaving openocd_riscv"
cd "$INSTALL_ROOT" || (echo "Failed to cd into installation root" && exit)
echo "openocd_riscv successfully installed"

echo "Entering xsvflib-pi directory"
cd iccfpga-utils/xsvflib-pi || (echo "Failed to cd into xsvflib-pi" && exit)
echo "Compile xsvflib-pi..."
make all
echo "Install xsvflib-pi..."
make install
echo "Leaving xsvflib-pi directory"
cd "$INSTALL_ROOT" || (echo "Failed to cd into installation root" && exit)

echo "Entering xvcpi (virtual cable server) directory"
cd iccfpga-utils/xvcpi || (echo "Failed to cd into xvcpi" && exit)
echo "Compile xvcpi..."
make all
echo "Install xvcpi..."
make install
echo "Leaving xvcpi..."
cd "$INSTALL_ROOT" || (echo "Failed to cd into installation root" && exit)
echo "xvcpi successfully installed"

echo "Entering iccfpga-rest-server"
cd iccfpga-rest-server || (echo "Failed to cd into iccfpga-rest-server" && exit)
echo "Compile Rest server..."
make
echo "Leaving Entering iccfpga-rest-server"
cd "$INSTALL_ROOT" || (echo "Failed to cd into installation root" && exit)
echo "iccfpga-rest-server successfully installed"

echo "Change boot/config.txt..."
echo "# risc-v reset to pull-up
gpio=19=pu

# fpga jtag
# tck
gpio=12=pn
# tms
gpio=16=pn
# tdi
gpio=20=pn
# tdo
gpio=21=pu" >> /boot/config.txt

echo "The device will be rebooted in 10 seconds..."
sleep 10s && reboot -h now